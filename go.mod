module gitlab.com/rhogenson/llama.cpp-webui

go 1.20

require (
	github.com/google/go-cmp v0.5.9
	nhooyr.io/websocket v1.8.7
)

require (
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
