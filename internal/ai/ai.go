// Package ai implements a wrapper around a connection to a llama.cpp
// example server.
package ai

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

// An AI knows how to connect to llama.cpp.
type AI struct {
	client        http.Client
	completionURL string
}

// New creates a client that will connect to the given llama.cpp server.
func New(server string) *AI {
	return &AI{
		completionURL: (&url.URL{
			Scheme: "http",
			Host:   server,
			Path:   "/completion",
		}).String(),
	}
}

type completeReq struct {
	Temperature float64  `json:"temperature"`
	NPredict    int      `json:"n_predict"`
	Stream      bool     `json:"stream"`
	Prompt      string   `json:"prompt"`
	Stop        []string `json:"stop"`
	PenalizeNL  bool     `json:"penalize_nl"`
	Mirostat    int      `json:"mirostat"`
}

// A CompleteOption is a parameter for AI text completion.
// See https://github.com/ggerganov/llama.cpp/tree/master/examples/server#api-endpoints
type CompleteOption func(*completeReq)

// Temperature adjusts the randomness of the generated text.
func Temperature(t float64) CompleteOption {
	return func(req *completeReq) {
		req.Temperature = t
	}
}

// NPredict sets the maximum number of tokens to predict.
func NPredict(n int) CompleteOption {
	return func(req *completeReq) {
		req.NPredict = n
	}
}

// Stop adds a reverse prompt that halts text generation.
func Stop(s string) CompleteOption {
	return func(req *completeReq) {
		req.Stop = append(req.Stop, s)
	}
}

// PenalizeNL sets whether to penalize newline tokens when applying the
// repeat penalty.
func PenalizeNL(b bool) CompleteOption {
	return func(req *completeReq) {
		req.PenalizeNL = b
	}
}

// Complete generates a completion for the given prompt and writes it one token at a time to the tokens channel.
//
// Expected usage:
//
//	ch := make(chan string)
//	var err error
//	go func() {
//	    err = client.Complete(ctx, ch, prompt)
//	    close(ch)
//	}()
//	for tok := range ch {
//	    // Process each token...
//	}
//	if err != nil {
//	    // ...
//	}
func (ai *AI) Complete(ctx context.Context, tokens chan<- string, prompt string, opts ...CompleteOption) error {
	req := completeReq{
		Temperature: 0.8,
		NPredict:    128,
		Stream:      true,
		Prompt:      prompt,
		PenalizeNL:  true,
		Mirostat:    2,
	}
	for _, opt := range opts {
		opt(&req)
	}
	jsonReq, err := json.Marshal(req)
	if err != nil {
		return fmt.Errorf("invalid request: %s", err)
	}

	httpReq, err := http.NewRequestWithContext(ctx, http.MethodPost, ai.completionURL, bytes.NewReader(jsonReq))
	if err != nil {
		return fmt.Errorf("build request: %s", err)
	}
	res, err := ai.client.Do(httpReq)
	if err != nil {
		return fmt.Errorf("llama.cpp: %s", err)
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("status %d from server", res.StatusCode)
	}

	body := bufio.NewScanner(res.Body)
	for body.Scan() {
		line := body.Bytes()
		if len(line) == 0 {
			continue
		}
		var token struct {
			Content string `json:"content"`
		}
		if err := json.Unmarshal(bytes.TrimPrefix(line, []byte("data: ")), &token); err != nil {
			log.Printf("Warning: invalid JSON from AI server: %s", err)
		}
		if token.Content != "" {
			tokens <- token.Content
		}
	}
	if err := body.Err(); err != nil {
		return fmt.Errorf("read response body: %s", err)
	}
	return nil
}
