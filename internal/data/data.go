// Package data implements a simple key-value store.
package data

import (
	"encoding/gob"
	"fmt"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

// Data implements a simple key-value store. Data is safe for concurrent use by
// multiple goroutines.
type Data struct {
	dir string
}

// New initializes the "database".
func New(dataDir string) (*Data, error) {
	if err := os.MkdirAll(dataDir, 0700); err != nil {
		return nil, fmt.Errorf("create data directory: %s", err)
	}
	return &Data{dir: filepath.Clean(dataDir) + "/"}, nil
}

// List lists the saved sessions.
func (d *Data) List() ([]string, error) {
	files, err := os.ReadDir(d.dir)
	if err != nil {
		return nil, fmt.Errorf("open data directory: %s", err)
	}
	names := make([]string, 0, len(files))
	for _, f := range files {
		fName := f.Name()
		if !strings.HasSuffix(fName, ".gob") {
			continue
		}
		name, err := url.PathUnescape(strings.TrimSuffix(fName, ".gob"))
		if err != nil {
			log.Printf("Warning: invalid saved session name %q.", fName)
			continue
		}
		names = append(names, name)
	}
	return names, nil
}

func (d *Data) filePath(name string) string {
	return d.dir + url.PathEscape(name) + ".gob"
}

// A Session stores a prompt and set of parameters.
type Session struct {
	// See https://github.com/ggerganov/llama.cpp/tree/master/examples/server#api-endpoints
	Prompt      string
	InPrefix    string
	InSuffix    string
	Temperature float64
	NPredict    int
	PenalizeNL  bool
}

// Load returns a saved session by name.
func (d *Data) Load(name string) (Session, error) {
	file, err := os.Open(d.filePath(name))
	if err != nil {
		return Session{}, fmt.Errorf("load: %s", err)
	}
	defer file.Close()
	var s Session
	if err := gob.NewDecoder(file).Decode(&s); err != nil {
		return Session{}, fmt.Errorf("invalid content: %s", err)
	}
	return s, nil
}

// Save saves a prompt with the given name.
func (d *Data) Save(name string, s Session) error {
	file, err := os.CreateTemp(d.dir, "*.temp")
	if err != nil {
		return fmt.Errorf("temp file: %s", err)
	}
	fileName := file.Name()
	if err := gob.NewEncoder(file).Encode(s); err != nil {
		file.Close()
		os.Remove(fileName)
		return fmt.Errorf("encode: %s", err)
	}
	if err := file.Close(); err != nil {
		os.Remove(fileName)
		return fmt.Errorf("write: %s", err)
	}
	if err := os.Rename(fileName, d.filePath(name)); err != nil {
		os.Remove(fileName)
		return fmt.Errorf("rename: %s", err)
	}
	return nil
}

// Delete removes a saved prompt.
func (d *Data) Delete(name string) error {
	return os.Remove(d.filePath(name))
}
