package data

import (
	"encoding/gob"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func newFake(t *testing.T) *Data {
	t.Helper()

	d, err := New(t.TempDir())
	if err != nil {
		t.Fatalf("Failed to create Data instance: %s", err)
	}
	return d
}

func writeSession(t *testing.T, path string, s Session) {
	t.Helper()

	file, err := os.Create(path)
	if err != nil {
		t.Fatalf("Failed to write test file: %s", err)
	}
	if err := gob.NewEncoder(file).Encode(s); err != nil {
		t.Fatalf("Failed to encode session: %s", err)
	}
	if err := file.Close(); err != nil {
		t.Fatalf("Failed to write session content: %s", err)
	}
}

func TestLoad(t *testing.T) {
	t.Parallel()

	data := newFake(t)
	writeSession(t, data.dir+"session1.gob", Session{
		Prompt:      "prompt",
		InPrefix:    "in-prefix",
		InSuffix:    "in-suffix",
		Temperature: 0.8,
		NPredict:    128,
		PenalizeNL:  true,
	})
	writeSession(t, data.dir+"session2.gob", Session{})

	const name = "session1"
	got, err := data.Load(name)
	if err != nil {
		t.Fatalf("Load(%q) failed: %s", name, err)
	}
	want := Session{
		Prompt:      "prompt",
		InPrefix:    "in-prefix",
		InSuffix:    "in-suffix",
		Temperature: 0.8,
		NPredict:    128,
		PenalizeNL:  true,
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Load(%q) returned unexpected diff (-want +got):\n%s", name, diff)
	}
}

func TestSave(t *testing.T) {
	t.Parallel()

	data := newFake(t)

	const name = "session"
	session := Session{
		Prompt:      "prompt",
		InPrefix:    "in-prefix",
		InSuffix:    "in-suffix",
		Temperature: 0.8,
		NPredict:    128,
		PenalizeNL:  true,
	}
	if err := data.Save(name, session); err != nil {
		t.Fatalf("Save(%q, %+v) failed: %s", name, session, err)
	}
	got, err := data.Load(name)
	if err != nil {
		t.Fatalf("Load(%q) failed: %s", name, err)
	}
	if diff := cmp.Diff(session, got); diff != "" {
		t.Errorf("Save(%q, %+v) saved an unexpected diff (-want +got):\n%s", name, session, diff)
	}
}

func TestList(t *testing.T) {
	t.Parallel()

	data := newFake(t)
	if err := data.Save("session1", Session{}); err != nil {
		t.Fatalf("Failed to seed test database: %s", err)
	}
	if err := data.Save("session2", Session{}); err != nil {
		t.Fatalf("Failed to seed test database: %s", err)
	}

	got, err := data.List()
	if err != nil {
		t.Fatalf("List failed: %s", err)
	}
	want := []string{"session1", "session2"}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("List returned unexpected diff (-want +got):\n%s", diff)
	}
}

func TestDelete(t *testing.T) {
	t.Parallel()

	data := newFake(t)
	if err := data.Save("session1", Session{}); err != nil {
		t.Fatalf("Failed to seed test database: %s", err)
	}
	if err := data.Save("session2", Session{}); err != nil {
		t.Fatalf("Failed to seed test database: %s", err)
	}

	const name = "session1"
	if err := data.Delete(name); err != nil {
		t.Fatalf("Delete(%q) failed: %s", name, err)
	}

	got, err := data.List()
	if err != nil {
		t.Fatalf("List failed: %s", err)
	}
	want := []string{"session2"}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Remaining sessions after Delete(%q) had unexpected diff (-want +got)\n%s", name, diff)
	}
}
