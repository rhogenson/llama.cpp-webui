// The llama.cpp-webui sever implements a terrible UI wrapper around
// llama.cpp's example server. It's like oobaboga/text-generation-webui, but
// I'm allergic to CSS.
package main

import (
	"context"
	"embed"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.com/rhogenson/llama.cpp-webui/internal/ai"
	"gitlab.com/rhogenson/llama.cpp-webui/internal/data"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

var (
	port     = flag.Int("port", 80, "Port number to bind to.")
	aiServer = flag.String("ai_server", "localhost:8080", "Address of the llama.cpp server.")
	store    = flag.String("store", "store", "Directory where llama.cpp-webui will store saved sessions.")

	//go:embed static
	staticFiles embed.FS

	//go:embed html/index.html.tmpl
	rawIndex string

	index = template.Must(template.New("index").Funcs(template.FuncMap{"empty": func() []string { return nil }}).Parse(rawIndex))
)

type server struct {
	client *ai.AI
	data   *data.Data
}

type indexArgs struct {
	SessionName string
	data.Session
}

func (*server) index(w http.ResponseWriter, _ *http.Request) {
	if err := index.Execute(w, indexArgs{
		Session: data.Session{
			Prompt:      "Below is an instruction that describes a task. Write a response that appropriately completes the request.\n\n### Instruction:\n",
			InPrefix:    "\n\n### Instruction:\n",
			InSuffix:    "\n\n### Response:\n",
			Temperature: 0.8,
			NPredict:    128,
			PenalizeNL:  true,
		},
	}); err != nil {
		log.Printf("Warning: failed to write page body: %s", err)
	}
}

func webSocketWrite(ctx context.Context, c *websocket.Conn, f func(io.Writer) error) error {
	w, err := c.Writer(ctx, websocket.MessageText)
	if err != nil {
		return err
	}
	if err := f(w); err != nil {
		w.Close()
		return err
	}
	return w.Close()
}

func (s *server) communicate(ctx context.Context, c *websocket.Conn) error {
	var req struct {
		Prompt      string `json:"prompt"`
		InPrefix    string `json:"in-prefix"`
		InSuffix    string `json:"in-suffix"`
		Temperature string `json:"temperature"`
		NPredict    string `json:"n-predict"`
		PenalizeNL  string `json:"penalize-nl"`
	}
	if err := wsjson.Read(ctx, c, &req); err != nil {
		return fmt.Errorf("read JSON from client: %w", err)
	}

	if err := webSocketWrite(ctx, c, func(w io.Writer) error {
		return index.ExecuteTemplate(w, "spinner", true)
	}); err != nil {
		return fmt.Errorf("send spinner: %w", err)
	}

	var diagnostics []string

	opts := []ai.CompleteOption{ai.PenalizeNL(req.PenalizeNL == "on")}
	stop := strings.TrimSpace(req.InPrefix)
	if stop != "" {
		opts = append(opts, ai.Stop(stop))
	}
	if temp, err := strconv.ParseFloat(req.Temperature, 64); err == nil {
		opts = append(opts, ai.Temperature(temp))
	} else {
		diagnostics = append(diagnostics, fmt.Sprintf("Warning: invalid temperature %q", req.Temperature))
	}
	if nPredict, err := strconv.Atoi(req.NPredict); err == nil {
		opts = append(opts, ai.NPredict(nPredict))
	} else {
		diagnostics = append(diagnostics, fmt.Sprintf("Warning: invalid n-predict %q", req.NPredict))
	}

	prompt := req.Prompt + req.InSuffix
	newContext := new(strings.Builder)
	newContext.WriteString(prompt)

	ch := make(chan string)
	var err error
	go func() {
		err = s.client.Complete(ctx, ch, prompt, opts...)
		close(ch)
	}()
	i := 0
	for tok := range ch {
		i++
		newContext.WriteString(tok)

		if i%10 == 0 {
			if err := webSocketWrite(ctx, c, func(w io.Writer) error {
				return index.ExecuteTemplate(w, "context", newContext)
			}); err != nil {
				return fmt.Errorf("send continuation: %w", err)
			}
		}
	}
	if err == nil {
		if !strings.HasSuffix(newContext.String(), stop) {
			newContext.WriteString(req.InPrefix)
		}
		if err := webSocketWrite(ctx, c, func(w io.Writer) error {
			return index.ExecuteTemplate(w, "context", newContext)
		}); err != nil {
			return fmt.Errorf("send final continuation: %w", err)
		}
	} else {
		diagnostics = append(diagnostics, fmt.Sprintf("Error: generate: %s", err))
	}

	if err := webSocketWrite(ctx, c, func(w io.Writer) error {
		if err := index.ExecuteTemplate(w, "errors", diagnostics); err != nil {
			return err
		}
		return index.ExecuteTemplate(w, "spinner", false)
	}); err != nil {
		return fmt.Errorf("send diagnostics: %w", err)
	}
	return nil
}

func (s *server) generate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	c, err := websocket.Accept(w, r, nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Printf("Warning: failed to make WebSocket handshake: %s", err)
		return
	}
	defer c.Close(websocket.StatusInternalError, "abnormal closure")

	for {
		err := s.communicate(ctx, c)
		if websocket.CloseStatus(err) == websocket.StatusGoingAway {
			return
		}
		if err != nil {
			log.Printf("Warning: failed to communicate with client: %s", err)
			c.Close(websocket.StatusTryAgainLater, "generation failed")
			return
		}
	}
}

func (s *server) saveErr(r *http.Request) error {
	name := r.FormValue("session-name")
	if name == "" {
		return errors.New("empty session name")
	}
	temp, _ := strconv.ParseFloat(r.FormValue("temperature"), 64)
	nPredict, _ := strconv.Atoi(r.FormValue("n-predict"))
	if err := s.data.Save(name, data.Session{
		Prompt:      r.FormValue("prompt"),
		InPrefix:    r.FormValue("in-prefix"),
		InSuffix:    r.FormValue("in-suffix"),
		Temperature: temp,
		NPredict:    nPredict,
		PenalizeNL:  r.FormValue("penalize-nl") == "on",
	}); err != nil {
		return fmt.Errorf("save session %q: %s", name, err)
	}
	return nil
}

func (s *server) save(w http.ResponseWriter, r *http.Request) {
	saved := true
	var errs []string
	if err := s.saveErr(r); err != nil {
		saved = false
		errs = []string{fmt.Sprintf("Error: %s", err)}
	}
	if err := index.ExecuteTemplate(w, "saved", saved); err != nil {
		log.Printf("Warning: failed to send back the saved marker: %s", err)
	}
	if err := index.ExecuteTemplate(w, "errors", errs); err != nil {
		log.Printf("Warning: failed to send errors: %s", err)
	}
}

var (
	//go:embed html/list.html.tmpl
	rawList string

	list = template.Must(template.New("list").Funcs(template.FuncMap{
		"pathEscape": url.PathEscape,
	}).Parse(rawList))
)

type listArgs struct {
	Sessions []string
	Error    string
}

func (s *server) listErr(w http.ResponseWriter, errMsg string) {
	sessions, err := s.data.List()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		if _, err := fmt.Fprintf(w, "Error: failed to list prompts: %s\n", err); err != nil {
			log.Printf("Warning: failed to send error message: %s", err)
			return
		}
		if errMsg != "" {
			if _, err := fmt.Fprintf(w, "%s\n", errMsg); err != nil {
				log.Printf("Warning: failed to send error message: %s", err)
			}
		}
		return
	}
	if err := list.Execute(w, listArgs{
		Sessions: sessions,
		Error:    errMsg,
	}); err != nil {
		log.Printf("Warning: failed to write list page: %s", err)
	}
}

func (s *server) list(w http.ResponseWriter, _ *http.Request) {
	s.listErr(w, "")
}

func (s *server) load(w http.ResponseWriter, r *http.Request) {
	name := strings.TrimPrefix(r.URL.Path, "/sessions/")
	session, err := s.data.Load(name)
	if err != nil {
		s.listErr(w, fmt.Sprintf("Error: failed to load session %q: %s", name, err))
		return
	}
	if err := index.Execute(w, indexArgs{SessionName: name, Session: session}); err != nil {
		log.Printf("Warning: failed to write prompt: %s", err)
	}
}

func (s *server) delete(w http.ResponseWriter, r *http.Request) {
	sessionName := strings.TrimPrefix(r.URL.Path, "/sessions/")
	errMsg := ""
	if err := s.data.Delete(sessionName); err != nil {
		if err := list.ExecuteTemplate(w, "element", sessionName); err != nil {
			log.Printf("Warning: failed to restore element: %s", err)
			return
		}
		errMsg = fmt.Sprintf("Error: failed to delete session %q: %s", sessionName, err)
	}
	if err := list.ExecuteTemplate(w, "error", errMsg); err != nil {
		log.Printf("Warning: failed to write error message: %s", err)
	}
}

func (s *server) register() {
	http.HandleFunc("/", s.index)
	http.HandleFunc("/generate", s.generate)
	http.HandleFunc("/sessions", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "PUT":
			s.save(w, r)
		case "GET":
			s.list(w, r)
		default:
			w.WriteHeader(http.StatusBadRequest)
		}
	})
	http.HandleFunc("/sessions/", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			s.load(w, r)
		case "DELETE":
			s.delete(w, r)
		default:
			w.WriteHeader(http.StatusBadRequest)
		}
	})
	http.Handle("/static/", http.FileServer(http.FS(staticFiles)))
}

func main() {
	flag.Parse()

	data, err := data.New(*store)
	if err != nil {
		log.Fatalf("Failed to initialize database: %s", err)
	}
	s := &server{
		client: ai.New(*aiServer),
		data:   data,
	}

	s.register()
	addr := fmt.Sprintf(":%d", *port)
	log.Printf("Info: serving on %s", addr)
	log.Fatal((&http.Server{Addr: addr, ReadHeaderTimeout: 3 * time.Second}).ListenAndServe())
}
